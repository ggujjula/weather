package com.ggujjula.weather.backend;

public class Location {
    public int id;
    public String streetNumber;
    public String street;
    public String city;
    public String state;
    public String longitude;
    public String latitude;
    public String wfoCallSign;
    public int gridpointX;
    public int gridpointY;

    public Location(int id){
        this.id = id;
        streetNumber = "123";
        street = "Main Street";
        city = "Austin";
        state = "TX";
        longitude = "123.234";
        latitude = "132.230";
        wfoCallSign = "ABC";
        gridpointX = 1;
        gridpointY = 984;
    }
}
