package com.ggujjula.weather.backend.nwsapi;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import java.util.regex.Matcher;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class NWSAPIService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_WFO_GET = "com.ggujjula.weather.backend.nwsapi.action.WFO_GET";
    private static final String ACTION_FORECAST_GET = "com.ggujjula.weather.backend.nwsapi.action.FORECAST_GET";
    private static final String ACTION_FORECAST_HOURLY_GET = "com.ggujjula.weather.backend.nwsapi.action.FORECAST_HOURLY_GET";

    private static final String EXTRA_ENDPOINT = "com.ggujjula.weather.backend.nwsapi.extra.ENDPOINT";
    private static final String EXTRA_LAT = "com.ggujjula.weather.backend.nwsapi.extra.LAT";
    private static final String EXTRA_LONG = "com.ggujjula.weather.backend.nwsapi.extra.LONG";
    private static final String EXTRA_WFO = "com.ggujjula.weather.backend.nwsapi.extra.WFO";
    private static final String EXTRA_X = "com.ggujjula.weather.backend.nwsapi.extra.X";
    private static final String EXTRA_Y = "com.ggujjula.weather.backend.nwsapi.extra.Y";


    public NWSAPIService() {
        super("NWSAPIService");
    }

    public static void startActionWFOGet(Context context, String lat, String longitude) {
        Intent intent = new Intent(context, NWSAPIService.class);
        intent.setAction(ACTION_WFO_GET);
        intent.putExtra(EXTRA_ENDPOINT, "https://api.weather.gov/points/<lat>,<long>");
        intent.putExtra(EXTRA_LAT, lat);
        intent.putExtra(EXTRA_LONG, longitude);
        context.startService(intent);
    }

    public static void startActionForecastGet(Context context, String wfo, String x, String y) {
        Intent intent = new Intent(context, NWSAPIService.class);
        intent.setAction(ACTION_FORECAST_GET);
        intent.putExtra(EXTRA_ENDPOINT, "https://api.weather.gov/gridpoints/<wfo>/<x>,<y>/forecast/");
        intent.putExtra(EXTRA_WFO, wfo);
        intent.putExtra(EXTRA_X, x);
        intent.putExtra(EXTRA_Y, y);
        context.startService(intent);
    }

    public static void startActionForecastHourlyGet(Context context, String wfo, String x, String y) {
        Intent intent = new Intent(context, NWSAPIService.class);
        intent.setAction(ACTION_FORECAST_HOURLY_GET);
        intent.putExtra(EXTRA_ENDPOINT, "https://api.weather.gov/gridpoints/<wfo>/<x>,<y>/forecast/hourly");
        intent.putExtra(EXTRA_WFO, wfo);
        intent.putExtra(EXTRA_X, x);
        intent.putExtra(EXTRA_Y, y);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_WFO_GET.equals(action)) {
                final String endpoint = intent.getStringExtra(EXTRA_ENDPOINT);
                final String lat = intent.getStringExtra(EXTRA_LAT);
                final String longitude = intent.getStringExtra(EXTRA_LONG);
                handleActionWFOGet(endpoint, lat, longitude);
            } else if (ACTION_FORECAST_GET.equals(action)) {
                final String endpoint = intent.getStringExtra(EXTRA_ENDPOINT);
                final String wfo = intent.getStringExtra(EXTRA_WFO);
                final String x = intent.getStringExtra(EXTRA_X);
                final String y = intent.getStringExtra(EXTRA_Y);
                handleActionForecastGet(endpoint, wfo, x, y);
            }
            else if (ACTION_FORECAST_HOURLY_GET.equals(action)) {
                final String endpoint = intent.getStringExtra(EXTRA_ENDPOINT);
                final String wfo = intent.getStringExtra(EXTRA_WFO);
                final String x = intent.getStringExtra(EXTRA_X);
                final String y = intent.getStringExtra(EXTRA_Y);
                handleActionForecastHourlyGet(endpoint, wfo, x, y);
            }
        }
    }

    private void handleActionWFOGet(String endpoint, String lat, String longitude) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void handleActionForecastGet(String endpoint, String wfo, String x, String y) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void handleActionForecastHourlyGet(String endpoint, String wfo, String x, String y) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
