package com.ggujjula.weather.ui.locationlist;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ggujjula.weather.R;
import com.ggujjula.weather.backend.Location;
import com.ggujjula.weather.ui.locationlist.LocationListFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Location} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyLocationListRecyclerViewAdapter extends RecyclerView.Adapter<MyLocationListRecyclerViewAdapter.ViewHolder> {

    private final List<Location> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyLocationListRecyclerViewAdapter(List<Location> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_location_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Location locationToBind = mValues.get(position);
        holder.mItem = locationToBind;
        holder.mCityState.setText(locationToBind.city + ", " + locationToBind.state);
        holder.mStreetAddress.setText(locationToBind.streetNumber + " " + locationToBind.street);
        holder.mLongitude.setText(locationToBind.longitude);
        holder.mLatitude.setText(locationToBind.latitude);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public CheckBox mDeleteButton;
        public final TextView mCityState;
        public final TextView mStreetAddress;
        public final TextView mLongitude;
        public final TextView mLatitude;
        public Location mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDeleteButton = (CheckBox) view.findViewById(R.id.deleteButton);
            mCityState = (TextView) view.findViewById(R.id.cityState);
            mStreetAddress = (TextView) view.findViewById(R.id.streetAddress);
            mLongitude = (TextView) view.findViewById(R.id.longitude);
            mLatitude = (TextView) view.findViewById(R.id.latitude);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mLongitude.getText() + " " + mLatitude.getText() + "'";
        }
    }
}
