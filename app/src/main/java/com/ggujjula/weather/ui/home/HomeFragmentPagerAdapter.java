package com.ggujjula.weather.ui.home;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class HomeFragmentPagerAdapter extends FragmentPagerAdapter {

    private static ArrayList<HomeFragment> testArray = null;
    private static final int arrayLength = 5;

    public HomeFragmentPagerAdapter(FragmentManager fm, int behavior){
        super(fm, behavior);
        testArray = new ArrayList<HomeFragment>(arrayLength);
        for(int i = 0; i < arrayLength; i++){
            testArray.add(new HomeFragment());
        }
    }

    @Override
    public int getCount(){
        return testArray.size();
    }

    @Override
    public Fragment getItem(int pos){
        return testArray.get(pos);
    }
}
