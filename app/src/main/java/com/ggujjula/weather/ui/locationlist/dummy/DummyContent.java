package com.ggujjula.weather.ui.locationlist.dummy;

import com.ggujjula.weather.backend.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Location> ITEMS = new ArrayList<Location>();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(Location item) {
        ITEMS.add(item);
    }

    private static Location createDummyItem(int position) {
        return new Location(position);
    }
}
